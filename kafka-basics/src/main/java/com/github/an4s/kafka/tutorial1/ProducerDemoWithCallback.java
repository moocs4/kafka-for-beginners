package com.github.an4s.kafka.tutorial1;

import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

public class ProducerDemoWithCallback {
    public static void main(String[] args) {

        Logger logger = LoggerFactory.getLogger(ProducerDemoWithCallback.class);

        Properties properties = new Properties();
        properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
        properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        KafkaProducer<String, String> producer = new KafkaProducer<String, String>(properties);

        for (int i = 1; i <= 10; i++) {
            ProducerRecord<String, String> record = new ProducerRecord<String, String>(
                    "first_topic", "Hello, World! from JavaWithCallback " + Integer.toString(i));

            producer.send(record, new Callback() {
                @Override
                public void onCompletion(RecordMetadata recordMetadata, Exception e) {
                    if (e == null) {
                        logger.info(
                                "Received new record.\n" +
                                        "Topic:\t" + recordMetadata.topic() + "\n" +
                                        "Partition:\t" + recordMetadata.partition() + "\n" +
                                        "Offset:\t" + recordMetadata.offset() + "\n" +
                                        "Timestamp:\t" + recordMetadata.timestamp()
                        );
                    } else {
                        logger.error("Error while producing:\t", e);
                    }
                }
            });
        }

        producer.close();
    }
}
