package com.github.an4s.kafka.tutorial4;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

public class StreamsFilterTweets {

    private final static Logger logger = LoggerFactory.getLogger(StreamsFilterTweets.class.getName());

    public static void main(String[] args) {
        Properties properties = new Properties();
        properties.setProperty(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
        properties.setProperty(StreamsConfig.APPLICATION_ID_CONFIG, "demo-kafka-streams");
        properties.setProperty(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.StringSerde.class.getName());
        properties.setProperty(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.StringSerde.class.getName());

        StreamsBuilder streamsBuilder = new StreamsBuilder();

        KStream<String, String> inputTopic = streamsBuilder.stream("twitter_tweets");
        KStream<String, String> filteredStream = inputTopic.filter(
                (k, jsonTweet) -> {
                    try {
                        return extractFollowersFromTweet(jsonTweet) > 10000;
                    } catch (Exception e) {
                        logger.error(e.toString());
                    }
                    return false;
                }
        );
        filteredStream.to("important_tweets");

        KafkaStreams kafkaStreams = new KafkaStreams(streamsBuilder.build(), properties);
        kafkaStreams.start();
    }

    private static int extractFollowersFromTweet(String tweetJSON) throws Exception {
        JSONObject jsonObject = new JSONObject(tweetJSON);
        if (jsonObject
                .has("includes") &&
            jsonObject
                    .getJSONObject("includes")
                    .has("users") &&
            jsonObject
                    .getJSONObject("includes")
                    .getJSONArray("users")
                    .length() > 0 &&
            jsonObject
                    .getJSONObject("includes")
                    .getJSONArray("users")
                    .getJSONObject(0)
                    .has("public_metrics") &&
            jsonObject
                    .getJSONObject("includes")
                    .getJSONArray("users")
                    .getJSONObject(0)
                    .getJSONObject("public_metrics")
                    .has("followers_count")) {
            return jsonObject
                    .getJSONObject("includes")
                    .getJSONArray("users")
                    .getJSONObject(0)
                    .getJSONObject("public_metrics")
                    .getInt("followers_count");
        }
        else {
            throw new Exception("followers_count not found");
        }
    }
}
